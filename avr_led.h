#include <avr/io.h>
//Emanuele Antonini
void led_init(void);

void turn_off_led(uint8_t mask);

void turn_on_led(uint8_t mask);

