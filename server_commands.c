#include "server_commands.h"

//Emanuele Antonini-Michele Amoroso

/* const uint8_t pinmasks[8]={
   0b00000001,
   0b00000010,
   0b00000100,
   0b00001000,
   0b00010000,
   0b00100000,
   0b01000000,
   0b10000000
   };
*/
uint8_t set_device_name(device* dev, uint8_t* dev_name) {
  
  //Caso in cui inserisco il primo device (la lista è vuota)
  if(dev->head == NULL) {
    dev->head = (device_node*)malloc(sizeof(device_node));
    dev->head->next = NULL;
    strcpy((char*)dev->head->dev_name, (char*)dev_name);
    dev->head->first_channel = NULL;
    UART_putString((uint8_t*)"[Server] Dispositivo creato!\n");
    return 1;
  }

  //Caso in cui ci sono già altri device nella lista
  device_node* list = dev->head;
  //Scorro la lista e nello stesso tempo controllo che il device non esista già
  while(list->next!=NULL){
    //Controllo esistenza device
    if(strcmp((char*)list->dev_name,(char*)dev_name)==0){
      UART_putString((uint8_t*)"[Server] ERRORE: Dispositivo già esistente\n");
      return -1;
    }
    list=list->next;
  }
  
  //Il ciclo while controlla n-1 elementi, devo controllare l'ultimo elemnto della lista
  if(strcmp((char*)list->dev_name,(char*)dev_name)==0){
    UART_putString((uint8_t*)"[Server] ERRORE: Dispositivo già esistente\n");
    return -1;
  }

  //In questo caso sono arrivato alla fine della lista, posso aggiungere il device in coda
  list->next=(device_node*)malloc(sizeof(device_node));
  list=list->next;
  strcpy((char*)list->dev_name,(char*)dev_name);
  list->first_channel=NULL;
  list->next=NULL;

  //Mando la stringa di successo al client
  UART_putString((uint8_t*)"[Server] Dispositivo creato!\n");
  return 1;
}


uint8_t get_all_devices_name(device* dev){
  device_node * list = dev->head;
  UART_putString((uint8_t*)"[Server] Ecco tutti i tuoi devices: \n");
  //Scorro la lista dei device e stampo i nomi
  while(list!=NULL){
    UART_putString((uint8_t*)"--");
    UART_putString(list->dev_name);
    UART_putString((uint8_t*)"\n");
    list=list->next;
  }
  UART_putString((uint8_t*)"STOP\n");
  return 1;
}


uint8_t set_channel_name(device* dev, uint8_t* dev_name, uint8_t* ch_name,uint8_t pinmask_index){
  
  //Se non ci sono devie (lista vuota) non ho nessun canale da stampare
  if(dev->head==NULL) {
    UART_putString((uint8_t*)"[Server] ERRORE: La lista dei device è vuota, crea un nuovo device prima di creare un canale\n");
    return -1;
  }
  
  //Prendo la testa della lista dei device
  device_node* list = dev->head;
  //Scorro la lista la lista dei device e cerco il device al quale voglio aggiungere il canale
  while(list!=NULL){
    //Se trovo il device aggiungo il canale
    if(strcmp((char*)list->dev_name, (char*)dev_name)==0){
      //Caso in cui il device non ha canali (lista dei canali vuota)
      if(list->first_channel == NULL) {
	list->first_channel = (channel*)malloc(sizeof(channel));
	list->first_channel->next = NULL;
	strcpy((char*)list->first_channel->ch_name, (char*)ch_name);
	list->first_channel->pinmask=from_index_to_mask(pinmask_index);
	list->first_channel->status=0;
	UART_putString((uint8_t*)"[Server] Canale creato!\n");
	return 1;
      }
      
      //Caso in cui ci sono già altri canali
      channel* ch_list = list->first_channel;
      //Scorro la lista dei canali e nel frattempo controllo che il canale già esista
      while(ch_list->next != NULL) {
	//Controllo che il canale esista già
	if(strcmp((char*)ch_list->ch_name,(char*)ch_name)==0){
	  UART_putString((uint8_t*)"[Server] ERRORE: Canale già esistente\n");
	  return -1;
	}
	ch_list = ch_list->next;
      }
      
      //Il ciclo while controlla n-1 elementi, devo controllare l'ultimo elemnto della lista
      if(strcmp((char*)ch_list->ch_name,(char*)ch_name)==0){
	UART_putString((uint8_t*)"[Server] ERRORE: Canale già esistente\n");
	return -1;
      }

      //In questo caso sono arrivato alla fine della lista, aggiungo il canale in coda
      ch_list->next = (channel*)malloc(sizeof(channel));
      strcpy((char*)ch_list->next->ch_name, (char*)ch_name);
      ch_list->next->pinmask=from_index_to_mask(pinmask_index);
      ch_list->next->status=0;
      ch_list->next->next = NULL;
      UART_putString((uint8_t*)"[Server] Canale creato!\n");
      return 1;
    }
    
    list = list->next;
  }

  //Se arrivo qui vuol dire che ho visitato tutta la lista dei device senza trova quello passato in input e quindi non esiste
  UART_putString((uint8_t*)"[Server] ERRORE: Dispositivo non esistente\n");
  return -1;
}


uint8_t get_channels_name_from_device(device* dev, uint8_t* dev_name){
  //Se non ci sono device non posso stampare i canali
  if(dev->head == NULL) {
    UART_putString((uint8_t*)"[Server] ERRORE: La lista dei device è vuota dunque non ci sono canali da mostrare\n");
    return -1;
  }
  
  device_node * list = dev->head;
  //Scorro la lista dei device
  while(list!=NULL){
    //If i find the device
    if(strcmp((char*)list->dev_name, (char*)dev_name)==0){
      //Controllo prima se il device ha almeno un canale
      if(list->first_channel == NULL) {
	UART_putString((uint8_t*)"[Server] ERRORE: Il device richiesto non ha canali\n");
	return -1;
      }
      
      //Il device ha almeno un canale, prendo il primo elemento della lista dei canali e lo metto in ch_list
      channel* ch_list = list->first_channel;
      UART_putString((uint8_t*)"OKAY\n"); //SE TUTTO VA A BUON FINE, CIOE' SE TROVO IL DEVICE E NON E' VUOTO ALLORA MANDO "OKAY" AL CLIENT PER AVVERTIRLO CHE DEVE LEGGERE UNA SERIE DI NOMI (CANALI)
      UART_putString((uint8_t*)"[Server] Ecco tutti i canali del device ");
      UART_putString((uint8_t*)list->dev_name);
      
      //Scorro la lista dei canali e li stampo
      while(ch_list != NULL){
	UART_putString((uint8_t*)"--");
	UART_putString((uint8_t*)ch_list->ch_name);
	UART_putString((uint8_t*)"\n");
	ch_list = ch_list->next;
      }
      UART_putString((uint8_t*)"STOP\n"); //AVVERTO IL CLIENT CHE HO FINITO I CANALI DA MOSTRARE
      return 1;
    }
    
    list = list->next;
  }

  //Se sono arrivato a questo punto vuol dire che non ho trovato nessun device con il nome dato in input
  UART_putString((uint8_t*)"[Server] ERRORE: Il device richiesto non esiste\n");
  return -1;
}


uint8_t free_channel_from_device(device* dev, uint8_t* dev_name, uint8_t* ch_name){
  uint8_t ret;
  //Se non ci sono device non ho canali da eliminare
  if(dev->head == NULL) {
    UART_putString((uint8_t*)"[Server] ERRORE: La lista dei device è vuota, non ci sono canali da eliminare\n");
    return -1;
  }

  device_node * list = dev->head;

  //Scorro la lista dei device
  while(list!=NULL){
    //Se trovo il device
    if(strcmp((char*)list->dev_name, (char*)dev_name)==0){
      //Controllo prima se il device ha canali
      if(list->first_channel == NULL) {
	UART_putString((uint8_t*)"[Server] ERRORE: Il device richiesto non ha canali\n");
	return -1;
      }

      //Se il canale da elminare è il primo della lista lo elimino e assegno come testa della lista dei canali il canale successivo
      if(strcmp((char*)list->first_channel->ch_name, (char*)ch_name)==0){
        channel* tmp = list->first_channel;
        list->first_channel = list->first_channel->next;
        uint8_t ret=tmp->pinmask;
	//Spengo il led nel caso sia acceso
        turn_off_led(~ret);
        free(tmp);
        reset_pin(from_mask_to_index(ret));
	UART_putString((uint8_t*)"[Server] Canale eliminato con successo!\n");
        return 1;
      }
      
      //Se il canale da eliminare non è il primo allora prendo il primo canale della lista e lo metto in ch_list
      channel* ch_list = list->first_channel;
      
      //Scorro la lista dei canali
      while(ch_list->next != NULL){
	//Se trovo il canale richiesto lo elimino
        if(strcmp((char*)ch_list->next->ch_name, (char*)ch_name)==0){
          channel* tmp = ch_list->next;
          ch_list->next = ch_list->next->next;
          ret=tmp->pinmask;
          turn_off_led(~ret);
          free(tmp);
          reset_pin(from_mask_to_index(ret));
	  UART_putString((uint8_t*)"[Server] Canale eliminato con successo!\n");
          return 1;
	}
	ch_list = ch_list->next;
      }
      //Se arrivo qui vuol dire che ho guuardato tutta la lista senza trovare il canale richiesto
      UART_putString((uint8_t*)"[Server] ERRORE: Non ci sono canali con il nome: ");
      UART_putString((uint8_t*)ch_name);
      //UART_putString((uint8_t*)"\n");
      return -1;
    }
    
    list = list->next;
  }

  //Se arrivo a questo punto vuol dire che ho guardato tutta la lista dei device senza trovare il device richiesto
  UART_putString((uint8_t*)"[Server] ERRORE: Non ci sono device con il nome: ");
  UART_putString((uint8_t*)dev_name);
  //UART_putString((uint8_t*)"\n");
  return -1;
      
}


uint8_t free_all_channels_from_device(device* dev, uint8_t* dev_name){
  //Se non ci sono device allora non ci sono canali da eliminare
  if(dev->head == NULL) {
    UART_putString((uint8_t*)"[Server] ERRORE: La lista dei device è vuota, non ci sono canali da eliminare\n");
    return -1;
  }

  device_node * list = dev->head;

  //Scorro la lista dei device
  while(list!=NULL){
    //Se trovo il device
    if(strcmp((char*)list->dev_name, (char*)dev_name)==0){
      //Controllo prima se il device ha almeno un canale
      if(list->first_channel == NULL) {
        UART_putString((uint8_t*)"[Server] ERRORE: Il device richiesto non ha canali\n");
        return -1;
      }

      //Scorro la lista dei canali e nel frattempo li elimino
      while(list->first_channel!=NULL){
        channel* tmp = list->first_channel;
        list->first_channel = list->first_channel->next;
        uint8_t ret=tmp->pinmask;
        turn_off_led(~ret);
        free(tmp);
        reset_pin(from_mask_to_index(ret));
      }
      return 1;
    }
    list = list->next;
  }

  //Se arrivo a questo punto vuol dire che non ho trovato device
  UART_putString((uint8_t*)"[Server] ERRORE: Non ci sono device con il nome: ");
  UART_putString((uint8_t*)dev_name);
  //UART_putString((uint8_t*)"\n");
  return -1;
}


uint8_t free_device(device* dev, uint8_t* dev_name){
  //Controllo prima se esiste almeno un device
  if(dev->head == NULL) {
    UART_putString((uint8_t*)"[Server] ERRORE: Non ci sono device da eliminare, la lista è vuota\n");
    return -1;
  }

  device_node * list = dev->head;

  //Caso in cui il device da eliminare è il primo
  if(strcmp((char*)list->dev_name, (char*)dev_name)==0){
    //Controllo se il device ha almento un canale
    if(dev->head->first_channel != NULL) {
      //Se ha almeno un canale prima di eliminare il device elimino tutti i canali associati
      while(dev->head->first_channel!=NULL){
        channel* tmp = dev->head->first_channel;
        dev->head->first_channel = dev->head->first_channel->next;
        uint8_t ret=tmp->pinmask;
        turn_off_led(~ret);
        free(tmp);
        reset_pin(from_mask_to_index(ret));
      }
    }

    //Arrivato qui sono sicuro che il device non ha più canali, posso eliminarlo
    device_node* tmp_dev = dev->head;
    dev->head = dev->head->next;
    free(tmp_dev);
    UART_putString((uint8_t*)"[Server] Device eliminato con successo!\n");
    return 1;
  }

  //Caso in cui il device da eliminare non è il primo
  //Scorro la lista dei device
  while(list->next!=NULL){
    //Se trovo il device
    if(strcmp((char*)list->next->dev_name, (char*)dev_name)==0){
      //Controllo se il device ha almento un canale
      if(list->next->first_channel != NULL) {
	//Se ha almeno un canale prima di eliminare il device elimino tutti i canali associati
        while(list->next->first_channel!=NULL){
          channel* tmp = list->first_channel;
          list->first_channel = list->first_channel->next;
          uint8_t ret=tmp->pinmask;
          turn_off_led(~ret);
          free(tmp);
          reset_pin(from_mask_to_index(ret));
        }
      }

      //Arrivato qui sono sicuro che il device non ha più canali, posso eliminarlo
      device_node* tmp_dev = list->next;
      list->next = list->next->next;
      free(tmp_dev);
      UART_putString((uint8_t*)"[Server] Device eliminato con successo!\n");
      return 1;
    }
    list = list->next;
  }

  //Se arrivo qui vuol dire che ho cercato su tutta la lista dei device senza trovare quello richiesto
  UART_putString((uint8_t*)"[Server] ERRORE: Non ci sono device con il nome: ");
  UART_putString((uint8_t*)dev_name);
  //UART_putString((uint8_t*)"\n");
  return -1;
}

//QUESTA FUNZIONE E' IDENTICA A free_device() SOLO NON HA GLI UART_putString() ED E' USATA DA reset_smarthouse(). SERVE PER NON INTERFERIRE CON IL CLIENT QUANDO VIENE CHIAMATA reset_smarthouse()
uint8_t free_device2(device* dev, uint8_t* dev_name){
  //Controllo prima se esiste almeno un device
  if(dev->head == NULL) {
    return -1;
  }

  device_node * list = dev->head;

  //Caso in cui il device da eliminare è il primo
  if(strcmp((char*)list->dev_name, (char*)dev_name)==0){
    //Controllo se il device ha almento un canale
    if(dev->head->first_channel != NULL) {
      //Se ha almeno un canale prima di eliminare il device elimino tutti i canali associati
      while(dev->head->first_channel!=NULL){
        channel* tmp = dev->head->first_channel;
        dev->head->first_channel = dev->head->first_channel->next;
        uint8_t ret=tmp->pinmask;
        turn_off_led(~ret);
        free(tmp);
        reset_pin(from_mask_to_index(ret));
      }
    }

    //Arrivato qui sono sicuro che il device non ha più canali, posso eliminarlo
    device_node* tmp_dev = dev->head;
    dev->head = dev->head->next;
    free(tmp_dev);
    return 1;
  }

  //Caso in cui il device da eliminare non è il primo
  //Scorro la lista dei device
  while(list->next!=NULL){
    //Se trovo il device
    if(strcmp((char*)list->next->dev_name, (char*)dev_name)==0){
      //Controllo se il device ha almento un canale
      if(list->next->first_channel != NULL) {
	//Se ha almeno un canale prima di eliminare il device elimino tutti i canali associati
        while(list->next->first_channel!=NULL){
          channel* tmp = list->first_channel;
          list->first_channel = list->first_channel->next;
          uint8_t ret=tmp->pinmask;
          turn_off_led(~ret);
          free(tmp);
          reset_pin(from_mask_to_index(ret));
        }
      }

      //Arrivato qui sono sicuro che il device non ha più canali, posso eliminarlo
      device_node* tmp_dev = list->next;
      list->next = list->next->next;
      free(tmp_dev);
      return 1;
    }
    list = list->next;
  }

  return -1;
}


uint8_t reset_smarthouse(device* dev){
  
  device_node* list = dev->head;

  //Scorro tutta la lista dei device e, con l'ausilio della funzione dichiarata in precedenza, elimino tutti i device della smarthouse
  while(list!=NULL){
    free_device2(dev, list->dev_name);
    list = list->next;
  }
  
  free(dev->head);
  UART_putString((uint8_t*)"[Server] smarthouse resettata con successo!\n");
  return 1;
}

uint8_t set_channel_status(device* dev, uint8_t* dev_name, uint8_t* ch_name,uint8_t status){
  //Prendo la testa della lista dei device
  device_node* list = dev->head;
  //Scorro la lista dei device
  while(list!=NULL){
    //Trovo il device richiesto
    if(strcmp((char*)list->dev_name, (char*)dev_name)==0){
      //Caso in cui il device non ha canali
      if(list->first_channel == NULL) {
	UART_putString((uint8_t*)"[Server] ERRORE: Non ci sono canali su questo dispositivo\n");
        return -1;
      }
      
      //Caso in cui il device ha almeno un canale
      channel* ch_list = list->first_channel;

      //Scorro la lista dei canali e cerco quello richiesto
      while(ch_list!= NULL) {
	//Se trovo il canale richiesto
	if(strcmp((char*)ch_list->ch_name,(char*)ch_name)==0){
	  //Controllo lo status inserito (1 = on | 0 = off) e setto il led di conseguenza
	  if(status==1) {
	    turn_on_led(ch_list->pinmask);
	    ch_list->status=1;
	    UART_putString((uint8_t*)"[Server] Status alterato\n");
	    return 1;
	  }
	  else {
            turn_off_led(~(ch_list->pinmask));
            ch_list->status=0;
	    UART_putString((uint8_t*)"[Server] Status alterato\n");
            return 0;
          }
          
	}
	ch_list = ch_list->next;
      }
      
      //Se arrivo qui vuol dire che non ho trovato il canale richiesto
      UART_putString((uint8_t*)"[Server] ERRORE: Il canale richiesto non è sul dispositivo\n");
      return -1;
    }
    list = list->next;
  }
  //Se arrivo qui vuol dire che non ho trovato il device richiesto
  UART_putString((uint8_t*)"[Server] ERRORE: Dispositivo non esistente\n");
  return -1;
}

int8_t get_channel_status(device* dev, uint8_t* dev_name, uint8_t* ch_name){
  //Prendo la testa della lista
  device_node* list = dev->head;
  //Scorro tutta la lista e cerco il device richiesto
  while(list!=NULL){
    //Se trovo il device
    if(strcmp((char*)list->dev_name, (char*)dev_name)==0){
      //Caso in cui il device non ha canali
      if(list->first_channel == NULL) {
	UART_putString((uint8_t*)"[Server] ERRORE: Il device non ha canali\n");
        return -1;
      }
      
      //Caso in cui ci sono già altri canali
      channel* ch_list = list->first_channel;

      //Scorro la lista dei canali
      while(ch_list->next != NULL) {
	//Se trovo il canale stampo il suo status
	if(strcmp((char*)ch_list->ch_name,(char*)ch_name)==0){
	  UART_putString((uint8_t*)"[Server] Lo status del canale richiesto è: ");
          if(ch_list->status==1) UART_putString((uint8_t*)"on\n");
          else UART_putString((uint8_t*)"off\n");
          //UART_putString((uint8_t*)"\n");
          return 1;
	}
	ch_list = ch_list->next;
      }
      
      //Il ciclo while controlla n-1 elementi, devo controllare l'ultimo
      if(strcmp((char*)ch_list->ch_name,(char*)ch_name)==0){
	if(strcmp((char*)ch_list->ch_name,(char*)ch_name)==0){
	  UART_putString((uint8_t*)"[Server] Lo status del canale richiesto è: ");
          if(ch_list->status==1) UART_putString((uint8_t*)"on\n");
          else UART_putString((uint8_t*)"off\n");
          //UART_putString((uint8_t*)"\n");
          return 1;
	}
      }

      //Se arrivo qui vuol dire che non ho trovato il canale richiesto
      UART_putString((uint8_t*)"[Server] ERRORE: Il canale richiesto non è sul device\n");
      return -1;
    }
    list = list->next;
  }

  //Se arrivo qui vuol dire che non ho trovato il device richiesto
  UART_putString((uint8_t*)"[Server] ERRRORE: Il device richiesto non esiste\n");
  return -1;
}

int8_t smarthouse_overview(device* dev){
  //Prendo la testa della lista
  device_node* list = dev->head;
  //Scorro tutta la lista dei device
  while(list!=NULL){
    //Stampo il device
    UART_putString((uint8_t*)"--");
    //PER POTER SCRIVERE "nome_device: " SENZA CHE I DUE PUNTI VADANO A CAPO TOLGO DAL dev_name LO '\n' IN MODO TALE CHE NON VADA A CAPO
    char device_name[15];
    strcpy(device_name, (char*)list->dev_name);
    int i = 0;
    while(device_name[i] != '\n'){
      i++;
    }
    device_name[i] = '\0';
    //Stampo il nome del device
    UART_putString((uint8_t*)device_name);
    //E vicino stampo i due punti (da notare che se non toglevo lo '\n' dal dev_name i due punti sarebbero stati stampati a capo)
    UART_putString((uint8_t*)":\n");
    //Per ogni device prendo la lista dei canali
    channel* ch_list = list->first_channel;
    //Scorro la lista dei canali e li stampo scrivendo anche il loro status
    while(ch_list != NULL) {
      UART_putString((uint8_t*)"  --");
      //PER EVITARE CHE UNA VOLTA SCRITTO IL NOME DEL CANALE VADA CAPO E SCRIVA " -> off/on" SOTTO AL NOME DEL CANALE, CREO UNA COPIA DEL NOME DEL CANALE E LEVO LO '\n' ALLA FINE DEL NOME.
      //IN QUESTO MODO POSSO SCRIVERE IL NOME DEL CANALE E ACCANTO SCRIVERE SE E' on O off
      char channel_name[15];
      strcpy(channel_name, (char*)ch_list->ch_name);
      int i = 0;
      while(channel_name[i] != '\n'){
	i++;
      }
      channel_name[i] = '\0';
      //Adesso stampo il nome del canale
      UART_putString((uint8_t*)channel_name);
      //Vicino al nome del canale verrà stampato on o off in base al suo status
      if(ch_list->status==1) UART_putString((uint8_t*)" -> on\n");
      else UART_putString((uint8_t*)" -> off\n");
      ch_list = ch_list->next;
    }
    list = list->next;
  }
  //Invio "STOP" al client per avvertirlo che sono finiti i device e canali da stampare
  UART_putString((uint8_t*)"STOP\n");
  return 1;
}
