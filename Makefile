# AVR makefile stub

# to clean a folder
# from command line
#$> make clean

# to build the folder
#$> make 

# to upload a file
# from command line
# $> make <main_filename>.hex

#
# remember to give yourself the read/write permissions on the
# serial line
# $> sudo addgroup <your username> dialout
# reboot



# !!!!!!!!!!!!!!!! MODIFY HERE !!!!!!!!!!!!!!!! 

# put here the file containing the main routine
# to be uploaded on the avr
# you can add multiple files, they will be all generated

BINS= server.elf

# put here the additional .o files you want to generate
# one .c file for each .o should be present
OBJS=avr_uart.o avr_led.o avr_init.o server_commands.o 

# put here the additional header files needed for compilation
HEADERS=avr_uart.h avr_led.h avr_init.h utils.h  server_commands.h

# the file below contains the actual rules

include ./avr.mk

# !!!!!!!!!!!!!!!! MODIFY HERE !!!!!!!!!!!!!!!! 



