#include "avr_init.h"
#include "server_commands.h"
#include <stdlib.h>
#include <avr/interrupt.h>
#include <util/delay.h>



uint8_t go=1;
int8_t res=0;
uint8_t command[2];
uint8_t dev_n[15];
uint8_t chan_n[15];
uint8_t chan_stat[15];
device* dev;


ISR(USART0_RX_vect, ISR_BLOCK){ // ISR(vector, attributes)
  // UART_putString((uint8_t*)"Avvio processo in interrupt:\n");
  UART_getString(command);
  switch(command[0]){
  case 49:                        //CREATE NEW DEVICE
    UART_putString((uint8_t*)"[Server] Inserisci il nome del nuovo dispositivo(max 15 chars):\n");
    UART_getString(dev_n);
    res=set_device_name(dev, dev_n);
    break;

  case 50:               
    res=find_first_free_pin();
    uint8_t ret;
    UART_putString((uint8_t*)"[Server] In quale device vuoi creare il canale?\n");
    UART_getString(dev_n);
    UART_putString((uint8_t*)"[Server] Inserici il nome del canale:\n");
    UART_getString(chan_n);
    if(res!=-1){
      ret=set_channel_name(dev,dev_n, chan_n,res);
      if(ret!=1){
	reset_pin(res);
      }
      break;
    }
    UART_putString((uint8_t*)"[Server] CREAZIONE DEL CANALE NON RIUSCITA: Numero massimo di canali raggiunto\n");
    break;
  case 51:                        //SHOW ALL THE DEVICES
    get_all_devices_name(dev);
    break;
  case 52:                        //SHOW ALL THE CHANNELS OF A GIVEN DEVICE
    UART_putString((uint8_t*)"[Server] Inserisci il nome del device:\n"); 
    UART_getString(dev_n);
    get_channels_name_from_device(dev,dev_n);
    break;
  case 53:                        //SET CHANNEL STATUS
    UART_putString((uint8_t*)"[Server] Di quale device fa parte il canale? \n");
    UART_getString(dev_n);
    UART_putString((uint8_t*)"[Server] Inserisci il nome del canale:\n");
    UART_getString(chan_n);
    UART_putString((uint8_t*)"[Server] Inserisci lo status del canale (on or off):\n");
    UART_getString(chan_stat);
    if(strcmp((char*)chan_stat,"on\n")==0)
      set_channel_status(dev,dev_n,chan_n,1);
    else if(strcmp((char*)chan_stat,"off\n")==0)
      set_channel_status(dev,dev_n,chan_n,0);
    else
      UART_putString((uint8_t*)"[Server] ERRORE: Status non valido\n");
    break;
  case 54:                        //GET CHANNEL STATUS
    UART_putString((uint8_t*)"[Server] Di quale device fa parte il canale? \n");
    UART_getString(dev_n);
    UART_putString((uint8_t*)"[Server] Inserisci il nome del canale:\n");
    UART_getString(chan_n);
    get_channel_status(dev, dev_n, chan_n);
    break;
  case 55:                        //FREE A DEVICE
    UART_putString((uint8_t*)"[Server] Quale device vuoi eliminare?\n");
    UART_getString(dev_n);
    free_device(dev,dev_n); 
    break;
  case 56:                        //FREE A CHANNEL
    UART_putString((uint8_t*)"[Server] Di quale device fa parte il canale da eliminare? \n");
    UART_getString(dev_n);
    UART_putString((uint8_t*)"[Server] Inserisci il nome del canale:\n");
    UART_getString(chan_n);
    free_channel_from_device(dev,dev_n,chan_n); 
    break;
  case 57:                        //SMARTHOUSE OVERVIEW
    smarthouse_overview(dev);
    break;
  case 48:                       //RESET THE SMARTHOUSE     
    UART_putString((uint8_t*)"[Server] Ripristino della smarthouse...\n");
    reset_smarthouse(dev);
    reset_pins();
    break;
  }

}

int main(void){
  //uint8_t busy_channels=0b00000000;
  
  dev=(device*)malloc(sizeof(device));
  dev->head = NULL;
  avr_init();

  //Abilita interrupts
  sei();

  while(1){
    _delay_ms(2000);
  }  
}
