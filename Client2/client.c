#include "client.h"

int fd; //this is the file descriptor corresponding to the serial port
int c_fd; //this is for serial_set_interface_attribs
char buf[BUF_LEN];
long milliseconds = 200;

//client main
int main(int argc, char* argv[]){
  int ret;        
  char *str[10];


  printf("Ciao Bello, benvenuto nella tua casa smart, ti è costata un sacco, per questo ti connetto subito all'Arduino Centrale\n");
     
  //opening corectly the serial port
  fd = serial_open("/dev/ttyACM0");
  c_fd = serial_set_interface_attribs(fd, BAUD_RATE);
  usleep(milliseconds * 1000);
  printf("Connessione in corso");
  for (int j = 0; j<1; j++) {
    for (int i = 0; i < 3; i++) {
      printf(".");
      fflush(stdout);
      sleep(1);
    }
    printf( "\b\b\b   \b\b\b");
    fflush(stdout);
  }

  printf("\nConnesso\n");
    

  //client main loop
  while(1){
      
    printf("\n" );

    printf("$> ");

    //clean the buffers
    memset(buf, 0, BUF_LEN);

    //read a line from shell [stdin] (fgets stores in buffer also newline symbol '/n')
    if (fgets(buf, sizeof(buf), stdin) != (char*)buf){
      fprintf(stderr, "Sorry, an error occured while reading your command. I'm exiting...\n");
      exit(EXIT_FAILURE);
    }

    printf("\n");

    
    if(!strncmp(buf, "set_device_name\n", strlen(buf))){ //SET_DEVICE_NAME

      //SCRIVO AL SERVER "1"
      ret = write_on_fd(fd, "1\n");
      usleep(milliseconds * 1000); //ASPETTO CHE ARDUINO SCRIVA SULLA SERIALE
	
      memset(buf, 0, BUF_LEN);
	
      //RICEVO DAL SERVER "Inserisci il nome del nuovo dispositivo:" 
      read_from_fd(fd, buf);
      usleep(milliseconds * 1000);

      printf("%s",buf);

      //clean the buffers
      memset(buf, 0, BUF_LEN);

      
      //LEGGO IN INPUT LA RISPOSTA
      if (fgets(buf, sizeof(buf), stdin) != (char*)buf){
	fprintf(stderr, "Sorry, an error occured while reading your command. I'm exiting...\n");
	exit(EXIT_FAILURE);
      }

      usleep(milliseconds * 1000);
      
      //SCRIVO LA RISPOSTA AL SERVER
      ret = write_on_fd(fd, buf);
      usleep(milliseconds * 1000);

      memset(buf, 0, BUF_LEN);

      //IL SERVER MI RISPONDE CON "Creato!" IN CASO DI SUCCESSO O ALTRO IN CASSO DI FALLIMENTO
      read_from_fd(fd, buf);
      usleep(milliseconds * 1000);
      printf("%s",buf);
    }
    else if(!strncmp(buf, "set_channel_name\n", strlen(buf))){

      //SCRIVO AL SERVER "2"
      ret = write_on_fd(fd, "2\n");
      usleep(milliseconds * 1000);
	
      memset(buf, 0, BUF_LEN);

      //LEGGO DAL SERVER "Crea nuovo canale: per quale dispositivo?"
      read_from_fd(fd, buf);
      usleep(milliseconds * 1000);

      printf("%s",buf);

      //clean the buffers
      memset(buf, 0, BUF_LEN);


      //PRENDO LA RISPOSTA DEL DISPOSITIVO
      if (fgets(buf, sizeof(buf), stdin) != (char*)buf){
	fprintf(stderr, "Sorry, an error occured while reading your command. I'm exiting...\n");
	exit(EXIT_FAILURE);
      }

      //MANDO LA RISPOSTA AL SERVER
      ret = write_on_fd(fd, buf);
      usleep(milliseconds * 1000);

      memset(buf, 0, BUF_LEN);

      //LEGGO DAL SERVER "Per quale canale?"
      read_from_fd(fd, buf);
      usleep(milliseconds * 1000);

      printf("%s",buf);

      //clean the buffers
      memset(buf, 0, BUF_LEN);

      //PRENDO LA RISPOSTA SUL CANALE
      if (fgets(buf, sizeof(buf), stdin) != (char*)buf){
	fprintf(stderr, "Sorry, an error occured while reading your command. I'm exiting...\n");
	exit(EXIT_FAILURE);
      }
      
      //MANDO LA RISPOSTA AL SERVER
      ret = write_on_fd(fd, buf);
      usleep(milliseconds * 1000);
      
      memset(buf, 0, BUF_LEN);

      //LEGGO RISCONTRO FINALE SUL SUCCESSO O FALLIMENTO DELL'OPERAZIONE
      read_from_fd(fd, buf);
      usleep(milliseconds * 1000);

      printf("%s",buf);

      //clean the buffers
      memset(buf, 0, BUF_LEN);
    }
    else if(!strncmp(buf, "show_all_devices\n", strlen(buf))){ //MOSTRA TUTTI I DEVICES

      //SCRIVO AL SERVER "3"
      ret = write_on_fd(fd, "3\n");
      usleep(milliseconds * 1000);
	
      memset(buf, 0, BUF_LEN);
      
      //LEGGO "Heres all your devices:"
      read_from_fd(fd, buf);
      usleep(milliseconds * 1000);

      printf("%s\n",buf);

      //clean the buffers
      memset(buf, 0, BUF_LEN);

      //LEGGO IL PRIMO DEVICE SE MI RISPONDE STOP VUOL DIRE CHE NON HO DISPOSITIVI SENNO' RISPONDERA' "--nome_device\n"
      read_from_fd(fd, buf);
      usleep(milliseconds * 1000);
      
      while(strncmp(buf, "STOP\n", strlen(buf))){
	
	printf("%s",buf);
	
	//clean the buffers
	memset(buf, 0, BUF_LEN);

	//LEGGO GLI ALTRI DEVICE SE MI RISPONDE STOP VUOL DIRE CHE HO FINITO SENNO' "--nome_device\n"
	read_from_fd(fd, buf);
        usleep(milliseconds * 1000);

      }

      //clean the buffers
      memset(buf, 0, BUF_LEN);      
    }
    else if(!strncmp(buf, "show_all_channels\n", strlen(buf))){ //MOSTRA TUTTI I CANALI DI UN DEVICE

      //SCRIVO AL SERVER "3"
      ret = write_on_fd(fd, "4\n");
      usleep(milliseconds * 1000);
	
      memset(buf, 0, BUF_LEN);
      
      //LEGGO "Insert the device name:"
      read_from_fd(fd, buf);
      usleep(milliseconds * 1000);

      printf("%s",buf);

      //clean the buffers
      memset(buf, 0, BUF_LEN);

      //LEGGO DA TASTIERA IL NOME DEL DEVICE
      if (fgets(buf, sizeof(buf), stdin) != (char*)buf){
	fprintf(stderr, "Sorry, an error occured while reading your command. I'm exiting...\n");
	exit(EXIT_FAILURE);
      }

      //MANDO IL NOME AL SERVER
      ret = write_on_fd(fd, buf);
      usleep(milliseconds * 1000);

      memset(buf, 0, BUF_LEN);

      //LEGGO UNA SCRITTA DAL SERVER IN BASE A CIO' CHE MI MANDA FARO' COSE
      read_from_fd(fd, buf);
      usleep(milliseconds * 1000);

      
      if(!strncmp(buf, "OKAY\n", strlen(buf))){ //SE IL SERVER MI INVIA "OKAY" VUOL DIRE CHE HO TROVATO IL DEVICE SENZA PROBLEMI

	//clean the buffers
	memset(buf, 0, BUF_LEN);
	
	//LEGGO "Heres all your channels from device:"
	read_from_fd(fd, buf);
        usleep(milliseconds * 1000);
	
	printf("%s\n",buf);
	
	//clean the buffers
	memset(buf, 0, BUF_LEN);
	
	//LEGGO IL PRIMO DEVICE SE MI RISPONDE STOP VUOL DIRE CHE NON HO DISPOSITIVI SENNO' RISPONDERA' "--nome_device\n"
	read_from_fd(fd, buf);
        usleep(milliseconds * 1000);
	
	while(strncmp(buf, "STOP\n", strlen(buf))){
	  
	  printf("%s",buf);
	
	  //clean the buffers
	  memset(buf, 0, BUF_LEN);
	  
	  //LEGGO GLI ALTRI DEVICE SE MI RISPONDE STOP VUOL DIRE CHE HO FINITO SENNO' "--nome_canale\n"
	  read_from_fd(fd, buf);
	  usleep(milliseconds * 1000);
	  
	}
	
	//clean the buffers
	memset(buf, 0, BUF_LEN);
      }else{ //ALTRIMENTI HO RISCONTRATO UN PROBLEMA DI QUALCHE TIPO
	
	printf("%s",buf);
	
	//clean the buffers
	memset(buf, 0, BUF_LEN);
      }
      
    }
    else if(!strncmp(buf, "set_channel_status\n", strlen(buf))){ //SETTA LO STATUS DI UN CANALE

      //MANDO AL SERVER "5"
      ret = write_on_fd(fd, "5\n");
      usleep(milliseconds * 1000);
	
      memset(buf, 0, BUF_LEN);

      //LEGGO DAL SERVER "Insert the channel's name:\n";
      read_from_fd(fd, buf);
      usleep(milliseconds * 1000);

      printf("%s",buf);

      //clean the buffers
      memset(buf, 0, BUF_LEN);

      //PRENDO IL DISPOSITIVO
      if (fgets(buf, sizeof(buf), stdin) != (char*)buf){
	fprintf(stderr, "Sorry, an error occured while reading your command. I'm exiting...\n");
	exit(EXIT_FAILURE);
      }

      //MANDO IL DISPOSITIVO PRESO AL SERVER
      ret = write_on_fd(fd, buf);
      usleep(milliseconds * 1000);

      memset(buf, 0, BUF_LEN);

      //LEGGO "Insert channel's name:\n"
      read_from_fd(fd, buf);
      usleep(milliseconds * 1000);

      printf("%s",buf);

      //clean the buffers
      memset(buf, 0, BUF_LEN);

      //PRENDO IL NOME DEL CANALE
      if (fgets(buf, sizeof(buf), stdin) != (char*)buf){
	fprintf(stderr, "Sorry, an error occured while reading your command. I'm exiting...\n");
	exit(EXIT_FAILURE);
      }

      //MANDO IL NOME DEL CANALE AL SERVER
      ret = write_on_fd(fd, buf);
      usleep(milliseconds * 1000);

      memset(buf, 0, BUF_LEN);

      //LEGGO "Insert the channel's status :\n"
      read_from_fd(fd, buf);
      usleep(milliseconds * 1000);

      printf("%s",buf);

      //clean the buffers
      memset(buf, 0, BUF_LEN);

      //INSERISCO LO STATUS (on/off)
      if (fgets(buf, sizeof(buf), stdin) != (char*)buf){
	fprintf(stderr, "Sorry, an error occured while reading your command. I'm exiting...\n");
	exit(EXIT_FAILURE);
      }

      //LO INVIO
      ret = write_on_fd(fd, buf);
      usleep(milliseconds * 1000);

      memset(buf, 0, BUF_LEN);

      //LEGGO RISPOSTA DAL SERVER (SUCCESSO O INSUCCESSO)
      read_from_fd(fd, buf);
      usleep(milliseconds * 1000);

      printf("%s",buf);

      //clean the buffers
      memset(buf, 0, BUF_LEN);
    }
    else if(!strncmp(buf, "get_channel_status\n", strlen(buf))){ //VEDE LO STATUS DI UN CANALE
      //MANDO AL SERVER "6"
      ret = write_on_fd(fd, "6\n");
      usleep(milliseconds * 1000);
	
      memset(buf, 0, BUF_LEN);

      //LEGGO "From which device?"
      read_from_fd(fd, buf);
      usleep(milliseconds * 1000);

      printf("%s",buf);

      //clean the buffers
      memset(buf, 0, BUF_LEN);

      //SCRIVO IL DEVICE
      if (fgets(buf, sizeof(buf), stdin) != (char*)buf){
	fprintf(stderr, "Sorry, an error occured while reading your command. I'm exiting...\n");
	exit(EXIT_FAILURE);
      }

      //MANDO IL DEVICE
      ret = write_on_fd(fd, buf);
      usleep(milliseconds * 1000);

      memset(buf, 0, BUF_LEN);

      //LEGGO "Insert channel's name:"
      read_from_fd(fd, buf);
      usleep(milliseconds * 1000);

      printf("%s",buf);

      //clean the buffers
      memset(buf, 0, BUF_LEN);

      //SCRIVO IL CANALE
      if (fgets(buf, sizeof(buf), stdin) != (char*)buf){
	fprintf(stderr, "Sorry, an error occured while reading your command. I'm exiting...\n");
	exit(EXIT_FAILURE);
      }
      
      //MANDO IL CANALE
      ret = write_on_fd(fd, buf);
      usleep(milliseconds * 1000);
      
      memset(buf, 0, BUF_LEN);

      //LEGGO RISPOSTA POSITIVA O NEGATIVA IN BASE ALL'ESITO
      read_from_fd(fd, buf);
      usleep(milliseconds * 1000);

      printf("%s",buf);

      //clean the buffers
      memset(buf, 0, BUF_LEN);
    }
    else if(!strncmp(buf, "free_device\n", strlen(buf))){ //ELIMINA UN DEVICE
      //MANDO AL SERVER "7"
      ret = write_on_fd(fd, "7\n");
      usleep(milliseconds * 1000);
	
      memset(buf, 0, BUF_LEN);

      //LEGGO "From which device?"
      read_from_fd(fd, buf);
      usleep(milliseconds * 1000);

      printf("%s",buf);

      //clean the buffers
      memset(buf, 0, BUF_LEN);

      //SCRIVO IL DEVICE
      if (fgets(buf, sizeof(buf), stdin) != (char*)buf){
	fprintf(stderr, "Sorry, an error occured while reading your command. I'm exiting...\n");
	exit(EXIT_FAILURE);
      }

      //MANDO IL DEVICE
      ret = write_on_fd(fd, buf);
      usleep(milliseconds * 1000);

      memset(buf, 0, BUF_LEN);

      //LEGGO RISPOSTA POSITIVA O NEGATIVA IN BASE ALL'ESITO
      read_from_fd(fd, buf);
      usleep(milliseconds * 1000);

      printf("%s",buf);

      //clean the buffers
      memset(buf, 0, BUF_LEN);
    }
    else if(!strncmp(buf, "free_channel\n", strlen(buf))){ //ELIMINA UN CANALE
      //MANDO AL SERVER "8"
      ret = write_on_fd(fd, "8\n");
      usleep(milliseconds * 1000);
	
      memset(buf, 0, BUF_LEN);

      //LEGGO "From which device?"
      read_from_fd(fd, buf);
      usleep(milliseconds * 1000);

      printf("%s",buf);

      //clean the buffers
      memset(buf, 0, BUF_LEN);

      //SCRIVO IL DEVICE
      if (fgets(buf, sizeof(buf), stdin) != (char*)buf){
	fprintf(stderr, "Sorry, an error occured while reading your command. I'm exiting...\n");
	exit(EXIT_FAILURE);
      }

      //MANDO IL DEVICE
      ret = write_on_fd(fd, buf);
      usleep(milliseconds * 1000);

      memset(buf, 0, BUF_LEN);

      //LEGGO "Insert channel's name:"
      read_from_fd(fd, buf);
      usleep(milliseconds * 1000);

      printf("%s",buf);

      //clean the buffers
      memset(buf, 0, BUF_LEN);

      //SCRIVO IL CANALE
      if (fgets(buf, sizeof(buf), stdin) != (char*)buf){
	fprintf(stderr, "Sorry, an error occured while reading your command. I'm exiting...\n");
	exit(EXIT_FAILURE);
      }
      
      //MANDO IL CANALE
      ret = write_on_fd(fd, buf);
      usleep(milliseconds * 1000);

      memset(buf, 0, BUF_LEN);

      //LEGGO RISPOSTA POSITIVA O NEGATIVA IN BASE ALL'ESITO
      read_from_fd(fd, buf);
      usleep(milliseconds * 1000);

      printf("%s",buf);

      //clean the buffers
      memset(buf, 0, BUF_LEN);
    }
    else if(!strncmp(buf, "smarthouse_overview\n", strlen(buf))){
      //MANDO AL SERVER "9"
      ret = write_on_fd(fd, "9\n");
      usleep(milliseconds * 1000);
	
      memset(buf, 0, BUF_LEN);

      //LEGGO IL PRIMO DEVICE SE MI RISPONDE STOP VUOL DIRE CHE NON HO DISPOSITIVI SENNO' RISPONDERA' "--nome_device\n"
      read_from_fd(fd, buf);
      usleep(milliseconds * 1000);
      
      while(strncmp(buf, "STOP\n", strlen(buf))){
	
	printf("%s",buf);
	
	//clean the buffers
	memset(buf, 0, BUF_LEN);

	//LEGGO GLI ALTRI DEVICE SE MI RISPONDE STOP VUOL DIRE CHE HO FINITO SENNO' "--nome_device\n"
	read_from_fd(fd, buf);
        usleep(milliseconds * 1000);

      }

      //clean the buffers
      memset(buf, 0, BUF_LEN);
    }
    else if(!strncmp(buf, "reset_smarthouse\n", strlen(buf))){
      //MANDO AL SERVER "0"
      ret = write_on_fd(fd, "0\n");
      usleep(milliseconds * 1000);
	
      memset(buf, 0, BUF_LEN);

      //LEGGO "let's reset smarthouse"
      read_from_fd(fd, buf);
      usleep(milliseconds * 1000);

      printf("%s",buf);

      //clean the buffers
      memset(buf, 0, BUF_LEN);

      //LEGGO ESITO DEL RESET POSITIVO O NEGATIVO
      read_from_fd(fd, buf);
      usleep(milliseconds * 1000);

      printf("%s",buf);

      //clean the buffers
      memset(buf, 0, BUF_LEN);
    }
    else if(!strncmp(buf, "exit\n", strlen(buf))){
      break;
    }
    else if(!strncmp(buf, "help\n", strlen(buf))){
      printf("Ecco qui la documentazione:\n\n");
      printf("- set_device_name --> crea un nuovo dispositivo\n");
      printf("- set_channel_name --> crea un nuovo canale\n");
      printf("- show_all_devices --> mostra tutti i dispositivi creati\n");
      printf("- show_all_channels --> mostra tutti i canali di un dispositivo\n");
      printf("- set_channel_status --> setta lo status (on/off) di un canale\n");
      printf("- get_channel_status --> mostra lo status di un canale\n");
      printf("- free_device --> elimina un device\n");
      printf("- free_channel --> elimina un canale\n");
      printf("- smarthouse_overview --> mostra una panoramica della smarthouse\n");
      printf("- reset_smarthouse --> resetta la smarthouse\n");
      printf("- exit --> chiude il programma\n");
    }
    else{
      printf("Comando non valido digitare 'help' per per consultare la documentazione\n");
    }
  }
}
