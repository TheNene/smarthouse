//client smart house COATTO
#include "client.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

//Stefano Vecchi

int fd; //fd seriale
char buf[256];
char lc[256];
int ret;

int main(){
  char delim[] = " ";
  char *str[10];
  uint8_t cmd[1];
  char input[256];
  memset(buf, 0, 256);
  memset(lc, 0, 256);
  
  printf("Ciao Bello, benvenuto nella tua casa smart, ti è costata un sacco, per questo ti connetto subito all'Arduino Centrale\n");
    
  //TODO: connettere e aprire descrittore
  fd = open_ser("/dev/ttyACM0"); //impostare nome del device 
  int c_fd=serial_set_interface_attribs(fd,BAUD_RATE);
  if(fd<0) _exit(EXIT_FAILURE);

  //Non cancellare -> Schermata loading BELLA!

  printf("Connessione in corso");
  for (int j = 0; j<1; j++) {
        for (int i = 0; i < 3; i++) {
            printf(".");
            fflush(stdout);
            sleep(1);
        }
        printf( "\b\b\b   \b\b\b");
        fflush(stdout);
    }

  printf("\nConnesso\n");


//do while per riga di comando:

do{ 
  memset(buf, 0, 256);
  memset(lc, 0, 256);

  /* read_from_fd(fd, buf); */
  delay(1000);
  
  printf("[Server]: Attendo comando\n" );

  printf("$> ");
  
  
  if(fgets(input, 256, stdin)) {
    input[strcspn(input, "\n")] = '\0';
  }

  printf("\n");
  fflush(stdout);

  if(!*input){ //controlla l'input alla pressione del tasto invio se non è stato premuto nessun tasto
    printf("[Client]: Errore, nessun comando inviato! Digitare \"help\" per mostrare documentazione! \n");
    fflush(stdout);
  }

  if(!strcmp(input, "exit")){
    break;
  }

  char *ptr = strtok(input, delim); //iteratore
  int i = 0;

  while(ptr != NULL){
    str[i] = ptr; //salva dentro array le stringhe in input
		ptr = strtok(NULL, delim);
    i++;
  }

/*   if(!strncmp(str[0], "smart_house_host", strlen(str[0])) &&  !strncmp(str[1], "set_name", strlen(str[1]))){

    write_fd(fd, "49"); //mando il comando per scrivere nome x device

    read_from_fd(fd, buf); //ricevo risposta;

    printf("[Server]: %s",buf);

    fgets(lc, sizeof(lc), stdin);
  
    write_fd(fd, lc); //scrivo il nome sul fd

  } */

  if(!strncmp(str[0], "smart_house", strlen(str[0]))){ //si collega alla smart_house


    if(!strncmp(str[1], "set_device_name", strlen(str[1]))){

      memset(input, 0, 256);

      *cmd=49;

      write_fd(fd, cmd); //mando il comando per scrivere nome x device

      /* delay(1000);

      memset(buf, 0, 256);

      read_from_fd(fd, buf); //ricevo risposta; */

      printf("[Server]: Inserisci il nome del dispositivo: ");

      fgets(lc, sizeof(lc), stdin);
  
      write_fd(fd, lc); //scrivo il nome sul fd

      delay(1000);

      memset(buf, 0, 256);

      read_from_fd(fd, buf); //ricevo risposta;

      printf("[Server]: %s",buf);

      memset(buf, 0, 256);

      memset(lc, 0, 256);
 
    }

    //SET CHANNEL NAME
    if(!strncmp(str[1], "set_channel_name", strlen(str[1]))){
      //inviare comando per settare channel name
      //mandargli il nome del device
      //mandargli il nome del channel

      write_fd(fd, "50"); //mando il comando per scrivere nome x device

      read_from_fd(fd, buf); //ricevo risposta; //forse prende tutto

      printf("[Server]: %s",buf);

      memset(buf, 0, 256);

      fgets(lc, sizeof(lc), stdin);

      write_fd(fd, lc); //scrivo il nome del device sul fd

      read_from_fd(fd, buf); //ricevo "Insert new channel's name:\n"

      printf("[Server]: %s",buf);

      memset(lc, 0, 256);

      fgets(lc, sizeof(lc), stdin);

      write_fd(fd, lc); //scrivo il nome del channel sul fd

      }
    
      //SHOW ALL DEVICES
      else if(!strncmp(str[1], "show_all_devices", strlen(str[1]))){
        write_fd(fd, "51");

        read_from_fd(fd, buf);

        printf("[Server]: %s",buf);

      }

      //SHOW DEVICES CHANNELS
      else if(!strncmp(str[1], "show_devices_channels", strlen(str[1]))){
        write_fd(fd, "52");

        read_from_fd(fd, buf);

        printf("[Server]: %s",buf);

        fgets(lc, sizeof(lc), stdin);

        write_fd(fd, lc); //scrivo il nome del device sul fd

        memset(buf, 0, 256);

        read_from_fd(fd, buf); //ricevo lista channel per quel device

      }

      //SET CHANNEL STATUS
      else if(!strncmp(str[1], "set_channel_status", strlen(str[1]))){
      //inviare comando per settare channel name
      //ricevere risposta
      //mandargli il nome del device
      //ricevere risposta
      //mandargli il nome del channel
      //ricevere risposta
      //mandargli on off

      write_fd(fd, "53"); //mando il comando per attivare status

      read_from_fd(fd, buf); //ricevo risposta;

      printf("[Server]: %s",buf);

      memset(buf, 0, 256);

      fgets(lc, sizeof(lc), stdin);

      write_fd(fd, lc); //scrivo il nome del device sul fd

      read_from_fd(fd, buf); //ricevo "Insert the channel's status(on or off)"

      printf("[Server]: %s",buf);

      memset(lc, 0, 256);

      fgets(lc, sizeof(lc), stdin);

      write_fd(fd, lc); //scrivo il nome del channel sul fd

        }

      //SHOW SINGLE CHANNEL STATUS
      else if(!strncmp(str[1], "show_single_channel_status", strlen(str[1]))){
        write_fd(fd, "54"); //mando il comando per attivare status

        read_from_fd(fd, buf); //ricevo risposta;

        printf("[Server]: %s",buf);

        memset(buf, 0, 256);

        fgets(lc, sizeof(lc), stdin); 

        write_fd(fd, lc); //scrivo il nome del device sul fd

        memset(lc,0,256);

        read_from_fd(fd, buf); //ricevo "Insert the channel's status(on or off)"

        printf("[Server]: %s",buf);

        fgets(lc, sizeof(lc), stdin);

        write_fd(fd, lc); //scrivo il nome del channel sul fd     
      }

      //FREE CHANNEL
      else if(!strncmp(str[1], "free_channel", strlen(str[1]))){
        write_fd(fd, "55"); //manda 55

        read_from_fd(fd, buf); // from which device?

        printf("[Server]: %s",buf);

        memset(buf, 0, 256);

        fgets(lc, sizeof(lc), stdin);

        write_fd(fd, lc);

        memset(lc, 0, 256);

        read_from_fd(fd, buf);

        printf("[Server]: %s",buf);

        fgets(lc, sizeof(lc), stdin);

        write_fd(fd, lc);      
      }

      //FREE DEVICE
      else if(!strncmp(str[1], "free_device", strlen(str[1]))){
        write_fd(fd, "56"); // manda 56

        read_from_fd(fd, buf); //which device?

        printf("[Server]: %s", buf);

        memset(buf, 0, 256);

        fgets(lc, sizeof(lc), stdin);

        write_fd(fd, lc); //scrivo il nome del device sul fd     
      }

      //RESET
      else if(!strncmp(str[1], "reset", strlen(str[1]))){
        write_fd(fd, "57"); //mando il comando per attivare status  
      }

      //NULLA DEI PRECEDENTI
      else{
        printf("[Client]: Comando non valido! Digitare \"help\" per mostrare documentazione! \n");
        fflush(stdout);
        }
  }

  /* else if(!strncmp(str[0], "help", strlen(str[0]))){
    //TODO: stampare documentazione;

    printf("[Client]: Ecco a voi la documentazione\n");
    printf("[Client]: DOCUMENTAZIONE\n");

  } */

  else{ // nel caso in cui viene immesso un commando non valido (controllare se serve)
    printf("[Client]: Comando non valido! Digitare \"help\" per mostrare documentazione! \n");
    fflush(stdout);
  }}while(1);

 printf("[Client]: Grazie per aver utilizzato il nostro programma, un caloroso saluto da\nAmoroso Michele, Antonini Emanuele, Vecchi Stefano\n");
    fflush(stdout);
    return 1;


  


}

