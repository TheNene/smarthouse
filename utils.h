#include <stdint.h>
//Emanuele Antonini-Michele Amoroso


typedef struct channel{
    uint8_t ch_name[15];  //channel name
    uint8_t status;  //channel value
    uint8_t pinmask;   //TODO,pin mask 
    struct channel* next;
}channel;

typedef struct device{
  struct device_node* head;
}device;

typedef struct device_node{
    uint8_t dev_name[15];
    struct channel* first_channel;
    struct device_node* next;
}device_node;

int8_t find_first_free_pin(void);

uint8_t reset_pins(void);

void reset_pin(uint8_t pos);

uint8_t from_index_to_mask(uint8_t pos);

 uint8_t from_mask_to_index(uint8_t mask);

uint8_t* return_pins_status(uint8_t* pin_s);


