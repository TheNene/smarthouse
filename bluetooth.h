#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>
#include <string.h>


int open_bluetooth_connection(char* dest);

void bluetooth_send_message(int socket, char* message);

void close_bluetooth_connection(int socket);
