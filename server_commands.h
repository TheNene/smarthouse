#include "utils.h"
#include "avr_led.h"
#include "avr_uart.h"
#include <stdlib.h>
#include <string.h>

//Emanuele Antonini-Michele Amoroso
uint8_t set_device_name(device* dev, uint8_t* dev_name); //FATTO-

uint8_t set_channel_name(device* dev, uint8_t* dev_name, uint8_t* ch_name,uint8_t pinmask_index); //FATTO-

uint8_t set_channel_status(device* dev, uint8_t* dev_name, uint8_t* ch_name, uint8_t status);//FATTO-

int8_t get_channel_status(device* dev, uint8_t* dev_name, uint8_t* ch_name);

//uint8_t get_all_channels_status(device* dev, uint8_t* dev_name);

uint8_t get_channels_name_from_device(device* dev, uint8_t* dev_name); //FATTO-

uint8_t get_all_devices_name(device* dev); //FATTO-

uint8_t free_channel_from_device(device* dev, uint8_t* dev_name, uint8_t* ch_name); //FATTO-

uint8_t free_all_channels_from_device(device* dev, uint8_t* dev_name); //FATTO 

uint8_t free_device(device* dev, uint8_t* dev_name);//FATTO

uint8_t reset_smarthouse(device* dev); //FATTO-

uint8_t from_mask_to_index(uint8_t mask);

int8_t smarthouse_overview(device* dev);

uint8_t free_device2(device* dev, uint8_t* dev_name);
