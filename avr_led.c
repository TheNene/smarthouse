#include "avr_led.h"
//Emanuele Antonini
void led_init(void){ 
    const uint8_t mask=0b11111111; 
    DDRB |= mask;  //all pins as output(Register to choose)
    PORTB=0;       //all output pins are set to 0V
}

void turn_on_led(uint8_t mask){
    PORTB|=mask;
}

void turn_off_led(uint8_t mask){
    PORTB&=mask;
}

