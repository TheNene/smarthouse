#include "bluetooth.h"

//Michele Amoroso

int open_bluetooth_connection(char* dest){
  
  struct sockaddr_rc addr = { 0 };
  int s, status;

  // allocate a socket
  s = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);

  // set the connection parameters (who to connect to)
  addr.rc_family = AF_BLUETOOTH;
  addr.rc_channel = (uint8_t) 1;
  str2ba(dest, &addr.rc_bdaddr);

  // connect to server
  status = connect(s, (struct sockaddr *)&addr, sizeof(addr));
  if( status < 0 ) perror("Connection error");
  else printf("Established connection\n");

  return s;
}

void bluetooth_send_message(int socket, char* message){
  int status;
  // send a message
  status = write(socket, message, strlen(message));
  
  if( status < 0 ) perror("Error sending message via bluetooth");
  else printf("Message '%s' sent\n", message);

  // read data from the server
  status = read(socket, message, sizeof(message));
  if(status > 0 ) {
    printf("received [%s]\n", message);
  }
  return;
}

void close_bluetooth_connection(int socket){
  close(socket);
  return;
}


int main(){
  char dest[18] = "A0:57:E3:B8:29:02";
  //printf("Inserisci indirizzo destinazione: ");
  //scanf("%s", dest);
  int socket = open_bluetooth_connection(dest);
  int flag = 1;
  while(flag){
    char message[20];
    printf("Inserisci messaggio da inviare (stop per finire): ");
    scanf("%s", message);
    if(strcmp(message, "stop") == 0){
      flag = 0;
    }
    bluetooth_send_message(socket, message);
  }
 close_bluetooth_connection(socket);
  return 0;
}

