#include <stdint.h>
#include "utils.h"

uint8_t pins[8]={0};

const uint8_t pinmasks[8]={
  0b00000001,
  0b00000010,
  0b00000100,
  0b00001000,
  0b00010000,
  0b00100000,
  0b01000000,
  0b10000000
  };


int8_t find_first_free_pin(void){ //checks if there is at least one free channel
    int i;
    for(i=0;i<8;i++)
        if(pins[i]==0) {
            pins[i]=1;
            return i; //0 means free
        }
    return -1;
}

uint8_t reset_pins(void){ 
    int i;
    for(i=0;i<8;i++)
        pins[i]=0;
    return 1;
}

void reset_pin(uint8_t pos){
    pins[pos]=0;
}

uint8_t from_index_to_mask(uint8_t pos){
    return pinmasks[pos];
}

 uint8_t from_mask_to_index(uint8_t mask){
    int i;
    for(i=0;i<8;i++)
      if(mask==pinmasks[i]) return i;
    return -1;
  }

uint8_t* return_pins_status(uint8_t* pin_s){
    int i;
    for(i=0;i<8;i++){
        pin_s[i]=pins[i]+48;
    }
    return pin_s;
}